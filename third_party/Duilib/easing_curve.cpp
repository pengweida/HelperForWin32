#include "StdAfx.h"
#include "easing_curve.h"
#include <assert.h>

static bool isConfigFunction(EasingCurve::Type type)
{
  return type >= EasingCurve::InElastic
    && type <= EasingCurve::OutInBounce;
}

class EasingCurveFunction
{
public:
  enum Type { In, Out, InOut, OutIn };

  EasingCurveFunction(EasingCurveFunction::Type type = In, double period = 0.3, double amplitude = 1.0,
    double overshoot = 1.70158f)
    : _t(type), _p(period), _a(amplitude), _o(overshoot)
  { }
  virtual ~EasingCurveFunction() {}
  virtual double value(double t);
  virtual EasingCurveFunction *copy() const;
  bool operator==(const EasingCurveFunction& other);

  Type _t;
  double _p;
  double _a;
  double _o;
};

double EasingCurveFunction::value(double t)
{
  return t;
}

EasingCurveFunction *EasingCurveFunction::copy() const
{
  return new EasingCurveFunction(_t, _p, _a, _o);
}

bool EasingCurveFunction::operator==(const EasingCurveFunction& other)
{
  return _t == other._t &&
    _p == other._p &&
    _a == other._a &&
    _o == other._o;
}

#include "easing.cpp"

class EasingCurvePrivate
{
public:
  EasingCurvePrivate()
    : type(EasingCurve::Linear),
    config(0),
    func(&easeNone)
  { }
  ~EasingCurvePrivate() { delete config; }
  void setType_helper(EasingCurve::Type);

  EasingCurve::Type type;
  EasingCurveFunction *config;
  EasingCurve::EasingFunction func;
};

struct ElasticEase : public EasingCurveFunction
{
  ElasticEase(Type type)
    : EasingCurveFunction(type, double(0.3), double(1.0))
  { }

  EasingCurveFunction *copy() const
  {
    ElasticEase *rv = new ElasticEase(_t);
    rv->_p = _p;
    rv->_a = _a;
    return rv;
  }

  double value(double t)
  {
    double p = (_p < 0) ? 0.3f : _p;
    double a = (_a < 0) ? 1.0f : _a;
    switch (_t) {
      case In:
        return easeInElastic(t, a, p);
      case Out:
        return easeOutElastic(t, a, p);
      case InOut:
        return easeInOutElastic(t, a, p);
      case OutIn:
        return easeOutInElastic(t, a, p);
      default:
        return t;
    }
  }
};

struct BounceEase : public EasingCurveFunction
{
  BounceEase(Type type)
    : EasingCurveFunction(type, 0.3f, 1.0f)
  { }

  EasingCurveFunction *copy() const
  {
    BounceEase *rv = new BounceEase(_t);
    rv->_a = _a;
    return rv;
  }

  double value(double t)
  {
    double a = (_a < 0) ? 1.0f : _a;
    switch (_t) {
      case In:
        return easeInBounce(t, a);
      case Out:
        return easeOutBounce(t, a);
      case InOut:
        return easeInOutBounce(t, a);
      case OutIn:
        return easeOutInBounce(t, a);
      default:
        return t;
    }
  }
};

struct BackEase : public EasingCurveFunction
{
  BackEase(Type type)
    : EasingCurveFunction(type, 0.3f, 1.0f, 1.70158f)
  { }

  EasingCurveFunction *copy() const
  {
    BackEase *rv = new BackEase(_t);
    rv->_o = _o;
    return rv;
  }

  double value(double t)
  {
    double o = (_o < 0) ? 1.70158f : _o;
    switch (_t) {
      case In:
        return easeInBack(t, o);
      case Out:
        return easeOutBack(t, o);
      case InOut:
        return easeInOutBack(t, o);
      case OutIn:
        return easeOutInBack(t, o);
      default:
        return t;
    }
  }
};

static EasingCurve::EasingFunction curveToFunc(EasingCurve::Type curve)
{
  switch (curve) {
    case EasingCurve::Linear:
      return &easeNone;
    case EasingCurve::InQuad:
      return &easeInQuad;
    case EasingCurve::OutQuad:
      return &easeOutQuad;
    case EasingCurve::InOutQuad:
      return &easeInOutQuad;
    case EasingCurve::OutInQuad:
      return &easeOutInQuad;
    case EasingCurve::InCubic:
      return &easeInCubic;
    case EasingCurve::OutCubic:
      return &easeOutCubic;
    case EasingCurve::InOutCubic:
      return &easeInOutCubic;
    case EasingCurve::OutInCubic:
      return &easeOutInCubic;
    case EasingCurve::InQuart:
      return &easeInQuart;
    case EasingCurve::OutQuart:
      return &easeOutQuart;
    case EasingCurve::InOutQuart:
      return &easeInOutQuart;
    case EasingCurve::OutInQuart:
      return &easeOutInQuart;
    case EasingCurve::InQuint:
      return &easeInQuint;
    case EasingCurve::OutQuint:
      return &easeOutQuint;
    case EasingCurve::InOutQuint:
      return &easeInOutQuint;
    case EasingCurve::OutInQuint:
      return &easeOutInQuint;
    case EasingCurve::InSine:
      return &easeInSine;
    case EasingCurve::OutSine:
      return &easeOutSine;
    case EasingCurve::InOutSine:
      return &easeInOutSine;
    case EasingCurve::OutInSine:
      return &easeOutInSine;
    case EasingCurve::InExpo:
      return &easeInExpo;
    case EasingCurve::OutExpo:
      return &easeOutExpo;
    case EasingCurve::InOutExpo:
      return &easeInOutExpo;
    case EasingCurve::OutInExpo:
      return &easeOutInExpo;
    case EasingCurve::InCirc:
      return &easeInCirc;
    case EasingCurve::OutCirc:
      return &easeOutCirc;
    case EasingCurve::InOutCirc:
      return &easeInOutCirc;
    case EasingCurve::OutInCirc:
      return &easeOutInCirc;
      // Internal for, compatibility with QTimeLine only ??
    case EasingCurve::InCurve:
      return &easeInCurve;
    case EasingCurve::OutCurve:
      return &easeOutCurve;
    case EasingCurve::SineCurve:
      return &easeSineCurve;
    case EasingCurve::CosineCurve:
      return &easeCosineCurve;
    default:
      return 0;
  };
}

static EasingCurveFunction *curveToFunctionObject(EasingCurve::Type type)
{
  EasingCurveFunction *curveFunc = 0;
  switch (type) {
    case EasingCurve::InElastic:
      curveFunc = new ElasticEase(ElasticEase::In);
      break;
    case EasingCurve::OutElastic:
      curveFunc = new ElasticEase(ElasticEase::Out);
      break;
    case EasingCurve::InOutElastic:
      curveFunc = new ElasticEase(ElasticEase::InOut);
      break;
    case EasingCurve::OutInElastic:
      curveFunc = new ElasticEase(ElasticEase::OutIn);
      break;
    case EasingCurve::OutBounce:
      curveFunc = new BounceEase(BounceEase::Out);
      break;
    case EasingCurve::InBounce:
      curveFunc = new BounceEase(BounceEase::In);
      break;
    case EasingCurve::OutInBounce:
      curveFunc = new BounceEase(BounceEase::OutIn);
      break;
    case EasingCurve::InOutBounce:
      curveFunc = new BounceEase(BounceEase::InOut);
      break;
    case EasingCurve::InBack:
      curveFunc = new BackEase(BackEase::In);
      break;
    case EasingCurve::OutBack:
      curveFunc = new BackEase(BackEase::Out);
      break;
    case EasingCurve::InOutBack:
      curveFunc = new BackEase(BackEase::InOut);
      break;
    case EasingCurve::OutInBack:
      curveFunc = new BackEase(BackEase::OutIn);
      break;
    default:
      curveFunc = new EasingCurveFunction(EasingCurveFunction::In, 0.3f, 1.0f, 1.70158f);     // ###
  }

  return curveFunc;
}

/*!
  Constructs an easing curve of the given \a type.
 */
EasingCurve::EasingCurve(Type type)
  : d_ptr(new EasingCurvePrivate)
{
  setType(type);
}

/*!
  Construct a copy of \a other.
 */
EasingCurve::EasingCurve(const EasingCurve &other)
  : d_ptr(new EasingCurvePrivate)
{
  // ### non-atomic, requires malloc on shallow copy
  *d_ptr = *other.d_ptr;
  if (other.d_ptr->config)
    d_ptr->config = other.d_ptr->config->copy();
}

/*!
  Destructor.
 */

EasingCurve::~EasingCurve()
{
  delete d_ptr;
}

/*!
  Copy \a other.
 */
EasingCurve &EasingCurve::operator=(const EasingCurve &other)
{
  // ### non-atomic, requires malloc on shallow copy
  if (d_ptr->config) {
    delete d_ptr->config;
    d_ptr->config = 0;
  }

  *d_ptr = *other.d_ptr;
  if (other.d_ptr->config)
    d_ptr->config = other.d_ptr->config->copy();

  return *this;
}

/*!
  Compare this easing curve with \a other and returns true if they are
  equal. It will also compare the properties of a curve.
 */
bool EasingCurve::operator==(const EasingCurve &other) const
{
  bool res = d_ptr->func == other.d_ptr->func
    && d_ptr->type == other.d_ptr->type;
  if (res && d_ptr->config && other.d_ptr->config) {
    // catch the config content
    res = d_ptr->config->operator==(*(other.d_ptr->config));
  }
  return res;
}

/*!
  \fn bool QEasingCurve::operator!=(const QEasingCurve &other) const
  Compare this easing curve with \a other and returns true if they are not equal.
  It will also compare the properties of a curve.

  \sa operator==()
*/

/*!
  Returns the amplitude. This is not applicable for all curve types.
  It is only applicable for bounce and elastic curves (curves of type()
  QEasingCurve::InBounce, QEasingCurve::OutBounce, QEasingCurve::InOutBounce,
  QEasingCurve::OutInBounce, QEasingCurve::InElastic, QEasingCurve::OutElastic,
  QEasingCurve::InOutElastic or QEasingCurve::OutInElastic).
 */
double EasingCurve::amplitude() const
{
  return d_ptr->config ? d_ptr->config->_a : 1.0;
}

/*!
  Sets the amplitude to \a amplitude.

  This will set the amplitude of the bounce or the amplitude of the
  elastic "spring" effect. The higher the number, the higher the amplitude.
  \sa amplitude()
*/
void EasingCurve::setAmplitude(double amplitude)
{
  if (!d_ptr->config)
    d_ptr->config = curveToFunctionObject(d_ptr->type);
  d_ptr->config->_a = amplitude;
}

/*!
  Returns the period. This is not applicable for all curve types.
  It is only applicable if type() is QEasingCurve::InElastic, QEasingCurve::OutElastic,
  QEasingCurve::InOutElastic or QEasingCurve::OutInElastic.
 */
double EasingCurve::period() const
{
  return d_ptr->config ? d_ptr->config->_p : 0.3;
}

/*!
  Sets the period to \a period.
  Setting a small period value will give a high frequency of the curve. A
  large period will give it a small frequency.

  \sa period()
*/
void EasingCurve::setPeriod(double period)
{
  if (!d_ptr->config)
    d_ptr->config = curveToFunctionObject(d_ptr->type);
  d_ptr->config->_p = period;
}

/*!
  Returns the overshoot. This is not applicable for all curve types.
  It is only applicable if type() is QEasingCurve::InBack, QEasingCurve::OutBack,
  QEasingCurve::InOutBack or QEasingCurve::OutInBack.
 */
double EasingCurve::overshoot() const
{
  return d_ptr->config ? d_ptr->config->_o : 1.70158f;
}

/*!
  Sets the overshoot to \a overshoot.

  0 produces no overshoot, and the default value of 1.70158 produces an overshoot of 10 percent.

  \sa overshoot()
*/
void EasingCurve::setOvershoot(double overshoot)
{
  if (!d_ptr->config)
    d_ptr->config = curveToFunctionObject(d_ptr->type);
  d_ptr->config->_o = overshoot;
}

/*!
  Returns the type of the easing curve.
*/
EasingCurve::Type EasingCurve::type() const
{
  return d_ptr->type;
}

void EasingCurvePrivate::setType_helper(EasingCurve::Type newType)
{
  double amp = -1.0;
  double period = -1.0;
  double overshoot = -1.0;

  if (config) {
    amp = config->_a;
    period = config->_p;
    overshoot = config->_o;
    delete config;
    config = 0;
  }

  if (isConfigFunction(newType) || (amp != -1.0) || (period != -1.0) || (overshoot != -1.0)) {
    config = curveToFunctionObject(newType);
    if (amp != -1.0)
      config->_a = amp;
    if (period != -1.0)
      config->_p = period;
    if (overshoot != -1.0)
      config->_o = overshoot;
    func = 0;
  } else if (newType != EasingCurve::Custom) {
    func = curveToFunc(newType);
  }
  assert((func == 0) == (config != 0));
  type = newType;
}

/*!
  Sets the type of the easing curve to \a type.
*/
void EasingCurve::setType(Type type)
{
  if (d_ptr->type == type)
    return;
  assert(type >= Linear && type < NCurveTypes - 1);

  d_ptr->setType_helper(type);
}

/*!
  Sets a custom easing curve that is defined by the user in the function \a func.
  The signature of the function is double myEasingFunction(double progress),
  where \e progress and the return value is considered to be normalized between 0 and 1.
  (In some cases the return value can be outside that range)
  After calling this function type() will return QEasingCurve::Custom.
  \a func cannot be zero.

  \sa customType()
  \sa valueForProgress()
*/
void EasingCurve::setCustomType(EasingFunction func)
{
  assert(func);
  d_ptr->func = func;
  d_ptr->setType_helper(Custom);
}

/*!
  Returns the function pointer to the custom easing curve.
  If type() does not return QEasingCurve::Custom, this function
  will return 0.
*/
EasingCurve::EasingFunction EasingCurve::customType() const
{
  return d_ptr->type == Custom ? d_ptr->func : 0;
}

/*!
  Return the effective progress for the easing curve at \a progress.
  While  \a progress must be between 0 and 1, the returned effective progress
  can be outside those bounds. For instance, QEasingCurve::InBack will
  return negative values in the beginning of the function.
 */
double EasingCurve::valueForProgress(double progress) const
{
  progress = MAX(0, MIN(progress, 1));
  if (d_ptr->func)
    return d_ptr->func(progress);
  else if (d_ptr->config)
    return d_ptr->config->value(progress);
  else
    return progress;
}

ostream& operator<<(ostream& out, const EasingCurve &item)
{
  out << "type:" << item.d_ptr->type
    << "func:" << item.d_ptr->func;
  if (item.d_ptr->config) {
    out << "period:" << item.d_ptr->config->_p << endl
      << "amp:" << item.d_ptr->config->_a << endl
      << "overshoot:" << item.d_ptr->config->_o << endl;
  }
  return out;
}

