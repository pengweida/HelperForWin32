﻿#pragma once

#define DUI_DECLARE_WND_CLASS(WndClassName, WndClassStyle,szXmlFileName) \
  LPCTSTR GetWindowClassName() const override { return WndClassName; } \
  UINT GetClassStyle() const override { return WndClassStyle; } \
  CDuiString GetSkinFile() override { return szXmlFileName; } \
  CDuiString GetSkinFolder() override { return L""; }

#define DUI_DECLARE_LOAD_FROM_RES(ResId) \
  virtual UILIB_RESOURCETYPE GetResourceType() const {return UILIB_ZIPRESOURCE;}\
  virtual LPCTSTR GetResourceID() const{return MAKEINTRESOURCEW(ResId);}

//quick控件diy
#define BEGIN_DUICONTROL_CREATE() \
virtual CControlUI* CreateControl(LPCTSTR pstrClass) override{\
CDuiString tag_name_outter = pstrClass;\

#define DUICONTROL_CREATE(tag_name, ClassName) \
if (!tag_name_outter.CompareNoCase(tag_name)) \
return new ClassName;

#define END_DUICONTROL_CREATE() \
return NULL; \
}

#define END_DUICONTROL_CREATE_SUPER() \
return __super::CreateControl(pstrClass); \
}


class WindowImpl : public WindowImplBase
{
public:
  inline CPaintManagerUI& __PM() { return m_PaintManager; }

  template<typename N>
  CControlUI* FindControl(const N& clue)
  {
    return __PM().FindControl(clue);
  }

  template<typename N, typename V>
  bool FindControl(const N& clue, V*& control)
  {
    control = static_cast<V *>(__PM().FindControl(clue));
    return control != nullptr;
  }

  void SendNotify(CControlUI* pControl, LPCTSTR pstrMessage, WPARAM wParam = 0, LPARAM lParam = 0, bool bAsync = false)
  {
    return __PM().SendNotify(pControl, pstrMessage, wParam, lParam, bAsync);
  }

  BOOL ModifyStyle(
    _In_ DWORD dwRemove,
    _In_ DWORD dwAdd,
    _In_ UINT nFlags = 0) throw()
  {
    ATLASSERT(::IsWindow(m_hWnd));

    DWORD dwStyle = ::GetWindowLong(m_hWnd, GWL_STYLE);
    DWORD dwNewStyle = (dwStyle & ~dwRemove) | dwAdd;
    if (dwStyle == dwNewStyle)
      return TRUE;

    ::SetWindowLong(m_hWnd, GWL_STYLE, dwNewStyle);
    if (nFlags != 0)
    {
      ::SetWindowPos(m_hWnd, NULL, 0, 0, 0, 0,
        SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE | nFlags);
    }

    return TRUE;
  }

  BOOL ModifyStyleEx(
    _In_ DWORD dwRemove,
    _In_ DWORD dwAdd,
    _In_ UINT nFlags = 0) throw()
  {
    ATLASSERT(::IsWindow(m_hWnd));

    DWORD dwStyle = ::GetWindowLong(m_hWnd, GWL_EXSTYLE);
    DWORD dwNewStyle = (dwStyle & ~dwRemove) | dwAdd;
    if (dwStyle == dwNewStyle)
      return TRUE;

    ::SetWindowLong(m_hWnd, GWL_EXSTYLE, dwNewStyle);
    if (nFlags != 0)
    {
      ::SetWindowPos(m_hWnd, NULL, 0, 0, 0, 0,
        SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE | nFlags);
    }

    return TRUE;
  }

  void SetWindowPos(HWND wnd, HWND insert_after, RECT rec, UINT uFlag)
  {
    ::SetWindowPos(wnd, insert_after, rec.left, rec.top, rec.right - rec.left, rec.bottom - rec.top, uFlag);
  }

  void SetWindowPos(HWND insert_after, RECT rec, UINT uFlag)
  {
    ::SetWindowPos(m_hWnd, insert_after, rec.left, rec.top, rec.right - rec.left, rec.bottom - rec.top, uFlag);
  }

  bool IsVisible()
  {
    return !!::IsWindowVisible(m_hWnd);
  }

protected:
  WindowImpl() {}
  virtual ~WindowImpl() override {}
  virtual void OnInitWnd() {}
  virtual void OnPrepareKeyDown(UINT nVirtualKey) {}

  virtual void OnSelChanged(TNotifyUI& msg) {}

  void Notify(TNotifyUI& msg) override
  {
    if (msg.sType == DUI_MSGTYPE_SELECTCHANGED) {
      OnSelChanged(msg);
    }
    __super::Notify(msg);
  }

  virtual LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, bool& bHandled) override
  {
    if (uMsg == WM_KEYDOWN) {
      OnPrepareKeyDown(UINT(wParam));
    }
    return 0;
  }

  virtual LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override
  {
    SIZE szRoundCorner = __PM().GetRoundCorner();
#if defined(WIN32) && !defined(UNDER_CE)
    if (!::IsIconic(*this)) {
      // 修正不存在圆角情况下，窗口阴影不显示的BUG
      if (szRoundCorner.cx == 0 && szRoundCorner.cy == 0) {
        CDuiRect rcWnd;
        ::GetWindowRect(*this, &rcWnd);
        rcWnd.Offset(-rcWnd.left, -rcWnd.top);
        rcWnd.right++; rcWnd.bottom++;
        HRGN hRgn = ::CreateRectRgnIndirect(&rcWnd);
        ::SetWindowRgn(*this, hRgn, TRUE);
        ::DeleteObject(hRgn);
      } else {
        return WindowImplBase::OnSize(uMsg, wParam, lParam, bHandled);
      }
    }
#endif
    bHandled = FALSE;
    return 0;
  }

private:
  // 统一性原则，重命名InitWindow为OnInitWnd
  virtual void InitWindow() override final { OnInitWnd(); }
  // 屏蔽WindowImplBase默认按键，避免VK_ESCAPE触发WM_CLOSE
  virtual LRESULT ResponseDefaultKeyEvent(WPARAM wParam) override final { return 0; }
};


template<class T>
class WindowShadowAdapt : public T
{
  struct CWndShadowInitStatus : public CWndShadow
  {
    CWndShadowInitStatus() { m_Status = 0; }
    using CWndShadow::Update;
  };

public:
  WindowShadowAdapt(int size = 3, int sharp = 3, int dark = 50, int x_offset = 0, int y_offset = 0, COLORREF color = 0)
  {
    wnd_shadow_.SetSize(size);
    wnd_shadow_.SetSharpness(sharp);
    wnd_shadow_.SetDarkness(dark);
    wnd_shadow_.SetPosition(x_offset, y_offset);
    wnd_shadow_.SetColor(color);
  }
  virtual ~WindowShadowAdapt() override {}

  virtual void OnInitWnd() override
  {
    wnd_shadow_.Create(T::m_hWnd);
  }
  void UpdateShadow()
  {
    wnd_shadow_.Update(T::m_hWnd);
  }

  CWndShadowInitStatus wnd_shadow_;
};