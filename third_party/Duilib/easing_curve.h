#pragma once
#include <iosfwd>
#include <assert.h>
#include <functional>

#pragma warning(disable:4244)

#ifndef MIN
#define MIN(a,b) ((a)>(b) ?(b):(a))
#endif
#ifndef MAX
#define MAX(a,b) ((a)>(b) ?(a):(b))
#endif

using namespace std;

class EasingCurvePrivate;
class DUILIB_API EasingCurve
{
public:
  enum Type {
    Linear,
    InQuad, OutQuad, InOutQuad, OutInQuad,
    InCubic, OutCubic, InOutCubic, OutInCubic,
    InQuart, OutQuart, InOutQuart, OutInQuart,
    InQuint, OutQuint, InOutQuint, OutInQuint,
    InSine, OutSine, InOutSine, OutInSine,
    InExpo, OutExpo, InOutExpo, OutInExpo,
    InCirc, OutCirc, InOutCirc, OutInCirc,
    InElastic, OutElastic, InOutElastic, OutInElastic,
    InBack, OutBack, InOutBack, OutInBack,
    InBounce, OutBounce, InOutBounce, OutInBounce,
    InCurve, OutCurve, SineCurve, CosineCurve,
    Custom, NCurveTypes
  };

  EasingCurve(Type type = Linear);
  EasingCurve(const EasingCurve &other);
  ~EasingCurve();

  EasingCurve &operator=(const EasingCurve &other);
  bool operator==(const EasingCurve &other) const;
  inline bool operator!=(const EasingCurve &other) const
  {
    return !(this->operator==(other));
  }

  double amplitude() const;
  void setAmplitude(double amplitude);

  double period() const;
  void setPeriod(double period);

  double overshoot() const;
  void setOvershoot(double overshoot);

  Type type() const;
  void setType(Type type);
  typedef double(*EasingFunction)(double progress);
  void setCustomType(EasingFunction func);
  EasingFunction customType() const;

  double valueForProgress(double progress) const;
private:
  EasingCurvePrivate * d_ptr;
  friend  ostream& operator<<(ostream& out, const EasingCurve &item);
};

ostream &operator<<(ostream& out, const EasingCurve &item);

#include <chrono>
namespace Chrono = std::chrono;

class Animator {
  using Clock = Chrono::high_resolution_clock;
public:
  Animator(int from, int to, int duration/*ms*/, std::function<void(int)> fn = nullptr, EasingCurve::Type type = EasingCurve::Linear)
    :ec_(type), from_(from), to_(to), duration_(duration), fn_(fn)
  {
  }
  Animator() :ec_(EasingCurve::Linear) {}

  bool Step(int interval/*ms*/, bool forward = true)
  {
    assert(interval > 0);
    int time_now = current_time_ + (forward ? interval : (-interval));
    bool isFinished = forward ? time_now >= duration_ : time_now <= 0;
    time_now = forward ? MIN(time_now, duration_) : MAX(time_now, 0);
    int val = duration_ == 0 ? (forward ? to_ : from_) : from_ + (to_ - from_) *ec_.valueForProgress(time_now * 1. / duration_);
    current_time_ = time_now;
    if (fn_)fn_(val);
    return isFinished;
  }

  void ResetTimePoint()
  {
    time_point_ = Clock::now();
  }

  bool Step2(bool forward = true)
  {
    auto step_count = Chrono::duration_cast<Chrono::milliseconds>(Clock::now() - time_point_).count();
    time_point_ += Chrono::milliseconds{ step_count };
    return Step(step_count, forward);
  }

  int current_time_ = 0;	//当前时间点 ms
private:
  Clock::time_point time_point_;

  double from_ = 0;
  double to_ = 0;
  int duration_ = 0;  //ms
  std::function<void(int)> fn_ = nullptr;
  EasingCurve ec_;
public:
  void SetRange(int from_val, int to_value) { from_ = from_val; to_ = to_value; }
  void SetDuration(int val) { duration_ = val; }
  void SetFn(std::function<void(int)> val) { fn_ = val; }
  void SetType(EasingCurve::Type type) { ec_.setType(type); }
  void ResetCurrentTime() { current_time_ = 0; }
  void SetCurrentTime(double current_time) { current_time_ = current_time; }
  void Complete(bool state_forward) { SetCurrentTime(state_forward ? duration_ : 0); }
};