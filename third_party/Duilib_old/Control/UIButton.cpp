#include "stdafx.h"
#include "UIButton.h"

///////////////////////////////////////////////////////////////////////////////////////
DECLARE_HANDLE(HZIP);	// An HZIP identifies a zip file that has been opened
typedef DWORD ZRESULT;
typedef struct
{
  int index;                 // index of this file within the zip
  char name[MAX_PATH];       // filename within the zip
  DWORD attr;                // attributes, as in GetFileAttributes.
  FILETIME atime, ctime, mtime;// access, create, modify filetimes
  long comp_size;            // sizes of item, compressed and uncompressed. These
  long unc_size;             // may be -1 if not yet known (e.g. being streamed in)
} ZIPENTRY;
typedef struct
{
  int index;                 // index of this file within the zip
  TCHAR name[MAX_PATH];      // filename within the zip
  DWORD attr;                // attributes, as in GetFileAttributes.
  FILETIME atime, ctime, mtime;// access, create, modify filetimes
  long comp_size;            // sizes of item, compressed and uncompressed. These
  long unc_size;             // may be -1 if not yet known (e.g. being streamed in)
} ZIPENTRYW;
#define OpenZip OpenZipU
#define CloseZip(hz) CloseZipU(hz)
extern HZIP OpenZipU(void *z, unsigned int len, DWORD flags);
extern ZRESULT CloseZipU(HZIP hz);
#ifdef _UNICODE
#define ZIPENTRY ZIPENTRYW
#define GetZipItem GetZipItemW
#define FindZipItem FindZipItemW
#else
#define GetZipItem GetZipItemA
#define FindZipItem FindZipItemA
#endif
extern ZRESULT GetZipItemA(HZIP hz, int index, ZIPENTRY *ze);
extern ZRESULT GetZipItemW(HZIP hz, int index, ZIPENTRYW *ze);
extern ZRESULT FindZipItemA(HZIP hz, const TCHAR *name, bool ic, int *index, ZIPENTRY *ze);
extern ZRESULT FindZipItemW(HZIP hz, const TCHAR *name, bool ic, int *index, ZIPENTRYW *ze);
extern ZRESULT UnzipItem(HZIP hz, int index, void *dst, unsigned int len, DWORD flags);
///////////////////////////////////////////////////////////////////////////////////////

namespace DuiLib
{

  class CGifHelper {
  public:
    CGifHelper(UINT timer_id) :timer_id_(timer_id) {}
    ~CGifHelper()
    {
      ReapResource();
    }
    //清理资源
    void ReapResource()
    {
      if (m_pGifImage != NULL)
      {
        delete m_pGifImage;
        m_pGifImage = NULL;
      }

      if (m_pPropertyItem != NULL)
      {
        free(m_pPropertyItem);
        m_pPropertyItem = NULL;
      }
      m_nFrameCount = 0;
      m_nFramePosition = 0;
      is_frame_added_ = false;
      m_di_.Clear();
    }
    CDuiString GetImageFileName() { return m_di_.sDrawString; }
    //重新加载图片资源
    void ReloadFile(LPCTSTR pStrImage)
    {
      //清理资源
      ReapResource();
      InitGifImage(pStrImage);
    }
    void ResetFramePos() { m_nFramePosition = 0; }
    //绘制
    bool Draw(HDC hDc, CButtonUI *p_owner)
    {
      bool is_my_timer_out = p_owner->timer_arr[timer_id_ - EVENT_TIEM_ID - 1];
      if (is_my_timer_out)
        p_owner->timer_arr[timer_id_ - EVENT_TIEM_ID - 1] = false;
      RECT rcItem = p_owner->m_rcItem;
      if (!is_gif_)return CRenderEngine::DrawImage(hDc, p_owner->m_pManager, rcItem, p_owner->m_rcPaint, m_di_);
      //绘制gif
      GUID pageGuid = Gdiplus::FrameDimensionTime;
      Gdiplus::Graphics graphics(hDc);
      m_pGifImage->SelectActiveFrame(&pageGuid, (is_my_timer_out || !is_frame_added_) ? m_nFramePosition : (m_nFramePosition - 1));
      graphics.DrawImage(m_pGifImage, rcItem.left, rcItem.top, rcItem.right - rcItem.left, rcItem.bottom - rcItem.top);
      if (!p_owner->is_playing_) {
        is_frame_added_ = false;
        if (p_owner->is_stop_)
          m_nFramePosition = 0;
        return true;
      }
      if (is_my_timer_out || !is_frame_added_) {
        m_nFramePosition = (++m_nFramePosition) % m_nFrameCount;
        is_frame_added_ = true;
        long lPause = ((long*)m_pPropertyItem->value)[m_nFramePosition] * 10;
        if (lPause == 0) lPause = 100;
        p_owner->GetManager()->SetTimer(p_owner, timer_id_, lPause);
      }
      return true;
    }
    void SetStaticImageFade(int fade) { m_di_.uFade = fade; }
  private:
    void InitGifImage(LPCTSTR pstrGifPath)
    {
      m_di_.sDrawString = pstrGifPath;
      m_pGifImage = LoadGifFromFile(pstrGifPath);
      if (NULL == m_pGifImage) {
        is_gif_ = false;
        return;
      }
      UINT nCount = 0;
      nCount = m_pGifImage->GetFrameDimensionsCount();
      GUID* pDimensionIDs = new GUID[nCount];
      m_pGifImage->GetFrameDimensionsList(pDimensionIDs, nCount);
      m_nFrameCount = m_pGifImage->GetFrameCount(&pDimensionIDs[0]);
      int nSize = m_pGifImage->GetPropertyItemSize(PropertyTagFrameDelay);
      m_pPropertyItem = (Gdiplus::PropertyItem*) malloc(nSize);
      m_pGifImage->GetPropertyItem(PropertyTagFrameDelay, nSize, m_pPropertyItem);
      delete  pDimensionIDs;
      pDimensionIDs = NULL;
      is_gif_ = nSize > 0 && m_nFrameCount > 1;
    }
    Gdiplus::Image* LoadGifFromFile(LPCTSTR pstrGifPath)
    {
      LPBYTE pData = NULL;
      DWORD dwSize = 0;

      do
      {
        CDuiString sFile = CPaintManagerUI::GetResourcePath();
        if (CPaintManagerUI::GetResourceZip().IsEmpty()) {
          sFile += pstrGifPath;
          HANDLE hFile = ::CreateFile(sFile.GetData(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, \
            FILE_ATTRIBUTE_NORMAL, NULL);
          if (hFile == INVALID_HANDLE_VALUE) break;
          dwSize = ::GetFileSize(hFile, NULL);
          if (dwSize == 0) break;

          DWORD dwRead = 0;
          pData = new BYTE[dwSize];
          ::ReadFile(hFile, pData, dwSize, &dwRead, NULL);
          ::CloseHandle(hFile);

          if (dwRead != dwSize) {
            delete[] pData;
            pData = NULL;
            break;
          }
        } else {
          sFile += CPaintManagerUI::GetResourceZip();
          HZIP hz = NULL;
          if (CPaintManagerUI::IsCachedResourceZip()) hz = (HZIP)CPaintManagerUI::GetResourceZipHandle();
          else hz = OpenZip((void*)sFile.GetData(), 0, 2);
          if (hz == NULL) break;
          ZIPENTRY ze;
          int i;
          if (FindZipItem(hz, pstrGifPath, true, &i, &ze) != 0) break;
          dwSize = ze.unc_size;
          if (dwSize == 0) break;
          pData = new BYTE[dwSize];
          int res = UnzipItem(hz, i, pData, dwSize, 3);
          if (res != 0x00000000 && res != 0x00000600) {
            delete[] pData;
            pData = NULL;
            if (!CPaintManagerUI::IsCachedResourceZip()) CloseZip(hz);
            break;
          }
          if (!CPaintManagerUI::IsCachedResourceZip()) CloseZip(hz);
        }

      } while (0);

      while (!pData)
      {
        //读不到图片, 则直接去读取bitmap.m_lpstr指向的路径
        HANDLE hFile = ::CreateFile(pstrGifPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, \
          FILE_ATTRIBUTE_NORMAL, NULL);
        if (hFile == INVALID_HANDLE_VALUE) break;
        dwSize = ::GetFileSize(hFile, NULL);
        if (dwSize == 0) break;

        DWORD dwRead = 0;
        pData = new BYTE[dwSize];
        ::ReadFile(hFile, pData, dwSize, &dwRead, NULL);
        ::CloseHandle(hFile);

        if (dwRead != dwSize) {
          delete[] pData;
          pData = NULL;
        }
        break;
      }
      if (!pData)
      {
        return NULL;
      }

      Gdiplus::Image* pImage = LoadGifFromMemory(pData, dwSize);
      delete pData;
      return pImage;
    }
    Gdiplus::Image* LoadGifFromMemory(LPVOID pBuf, size_t dwSize)
    {
      HGLOBAL hMem = ::GlobalAlloc(GMEM_FIXED, dwSize);
      BYTE* pMem = (BYTE*)::GlobalLock(hMem);

      memcpy(pMem, pBuf, dwSize);

      IStream* pStm = NULL;
      ::CreateStreamOnHGlobal(hMem, TRUE, &pStm);
      Gdiplus::Image *pImg = Gdiplus::Image::FromStream(pStm);
      if (!pImg || pImg->GetLastStatus() != Gdiplus::Ok)
      {
        pStm->Release();
        ::GlobalUnlock(hMem);
        return 0;
      }
      return pImg;
    }
  private:
    Gdiplus::Image	*m_pGifImage = nullptr; //gdi图片资源
    UINT			m_nFrameCount = 0;				// gif图片总帧数
    UINT			m_nFramePosition = 0;			// 当前放到第几帧
    Gdiplus::PropertyItem*	m_pPropertyItem = nullptr;	// 帧与帧之间间隔时间
    bool is_gif_ = false; //是否是gif资源(gif资源只能给路径,不支持其他属性)
    bool is_frame_added_ = false;
    TDrawInfo m_di_;  //静态图资源
    UINT timer_id_;
  };




  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  CButtonUI::CButtonUI()
    : m_uButtonState(0)
    , m_dwHotTextColor(0)
    , m_dwPushedTextColor(0)
    , m_dwFocusedTextColor(0)
    , m_dwHotBkColor(0)
    , m_uFadeAlphaDelta(0)
    , m_uFadeAlpha(255)
  {
    m_uTextStyle = DT_SINGLELINE | DT_VCENTER | DT_CENTER;

    m_gifHelperNormal = new CGifHelper(EVENT_TIEM_ID + 1);
    m_gifHelperFore = new CGifHelper(EVENT_TIEM_ID + 2);
    m_gifHelperHot = new CGifHelper(EVENT_TIEM_ID + 3);
    m_gifHelperHotFore = new CGifHelper(EVENT_TIEM_ID + 4);
    m_gifHelperPushed = new CGifHelper(EVENT_TIEM_ID + 5);
    m_gifHelperPushedFore = new CGifHelper(EVENT_TIEM_ID + 6);
    m_gifHelperFocused = new CGifHelper(EVENT_TIEM_ID + 7);
    m_gifHelperDisabled = new CGifHelper(EVENT_TIEM_ID + 8);
  }

  CButtonUI::~CButtonUI()
  {
    delete m_gifHelperNormal;
    delete m_gifHelperFore;
    delete m_gifHelperHot;
    delete m_gifHelperHotFore;
    delete m_gifHelperPushed;
    delete m_gifHelperPushedFore;
    delete m_gifHelperFocused;
    delete m_gifHelperDisabled;
  }

  LPCTSTR CButtonUI::GetClass() const
  {
    return DUI_CTR_BUTTON;
  }

  LPVOID CButtonUI::GetInterface(LPCTSTR pstrName)
  {
    if (_tcscmp(pstrName, DUI_CTR_BUTTON) == 0) return static_cast<CButtonUI*>(this);
    return CLabelUI::GetInterface(pstrName);
  }

  UINT CButtonUI::GetControlFlags() const
  {
    return (IsKeyboardEnabled() ? UIFLAG_TABSTOP : 0) | (IsEnabled() ? UIFLAG_SETCURSOR : 0);
  }

  void CButtonUI::DoEvent(TEventUI& event)
  {
    if (!IsMouseEnabled() && event.Type > UIEVENT__MOUSEBEGIN && event.Type < UIEVENT__MOUSEEND) {
      if (m_pParent != NULL) m_pParent->DoEvent(event);
      else CLabelUI::DoEvent(event);
      return;
    }

    if (event.Type == UIEVENT_SETFOCUS)
    {
      Invalidate();
    }
    if (event.Type == UIEVENT_KILLFOCUS)
    {
      Invalidate();
    }
    if (event.Type == UIEVENT_KEYDOWN)
    {
      if (IsKeyboardEnabled() && IsEnabled()) {
        if (event.chKey == VK_SPACE || event.chKey == VK_RETURN) {
          Activate();
          return;
        }
      }
    }
    if (event.Type == UIEVENT_BUTTONDOWN || event.Type == UIEVENT_DBLCLICK)
    {
      if (::PtInRect(&m_rcItem, event.ptMouse) && IsEnabled()) {
        m_uButtonState |= UISTATE_PUSHED | UISTATE_CAPTURED;
        Invalidate();
      }
      return;
    }
    if (event.Type == UIEVENT_MOUSEMOVE)
    {
      if ((m_uButtonState & UISTATE_CAPTURED) != 0) {
        if (::PtInRect(&m_rcItem, event.ptMouse)) m_uButtonState |= UISTATE_PUSHED;
        else m_uButtonState &= ~UISTATE_PUSHED;
        Invalidate();
      }
      return;
    }
    if (event.Type == UIEVENT_BUTTONUP)
    {
      if ((m_uButtonState & UISTATE_CAPTURED) != 0) {
        if (::PtInRect(&m_rcItem, event.ptMouse) && IsEnabled()) Activate();
        m_uButtonState &= ~(UISTATE_PUSHED | UISTATE_CAPTURED);
        Invalidate();
      }
      return;
    }
    if (event.Type == UIEVENT_CONTEXTMENU)
    {
      if (IsContextMenuUsed() && IsEnabled()) {
        m_pManager->SendNotify(this, DUI_MSGTYPE_MENU, event.wParam, event.lParam);
      }
      return;
    }
    if (event.Type == UIEVENT_MOUSEENTER)
    {
      if (::PtInRect(&m_rcItem, event.ptMouse)) {
        if (IsEnabled()) {
          if ((m_uButtonState & UISTATE_HOT) == 0) {
            m_uButtonState |= UISTATE_HOT;
            Invalidate();
          }
        }
      }
      if (GetFadeAlphaDelta() > 0) {
        m_pManager->SetTimer(this, FADE_TIMERID, FADE_ELLAPSE);
      }
    }
    if (event.Type == UIEVENT_MOUSELEAVE)
    {
      if (!::PtInRect(&m_rcItem, event.ptMouse)) {
        if (IsEnabled()) {
          if ((m_uButtonState & UISTATE_HOT) != 0) {
            m_uButtonState &= ~UISTATE_HOT;
            Invalidate();
          }
        }
        if (m_pManager) m_pManager->RemoveMouseLeaveNeeded(this);
        if (GetFadeAlphaDelta() > 0) {
          m_pManager->SetTimer(this, FADE_TIMERID, FADE_ELLAPSE);
        }
      } else {
        if (m_pManager) m_pManager->AddMouseLeaveNeeded(this);
        return;
      }
    }
    if (event.Type == UIEVENT_SETCURSOR)
    {
      ::SetCursor(::LoadCursor(NULL, MAKEINTRESOURCE(IDC_HAND)));
      return;
    }
    if (event.Type == UIEVENT_TIMER && event.wParam == FADE_TIMERID)
    {
      if ((m_uButtonState & UISTATE_HOT) != 0) {
        if (m_uFadeAlpha > m_uFadeAlphaDelta) m_uFadeAlpha -= m_uFadeAlphaDelta;
        else {
          m_uFadeAlpha = 0;
          m_pManager->KillTimer(this, FADE_TIMERID);
        }
      } else {
        if (m_uFadeAlpha < 255 - m_uFadeAlphaDelta) m_uFadeAlpha += m_uFadeAlphaDelta;
        else {
          m_uFadeAlpha = 255;
          m_pManager->KillTimer(this, FADE_TIMERID);
        }
      }
      Invalidate();
      return;
    }
    if (event.Type == UIEVENT_TIMER && (event.wParam <= EVENT_TIEM_ID + 8 && event.wParam >= EVENT_TIEM_ID + 1) && event.pSender == this) {  //gif定时器
      timer_arr[event.wParam - EVENT_TIEM_ID - 1] = true;
      m_pManager->KillTimer(this, event.wParam);
      Invalidate();
    }
    CLabelUI::DoEvent(event);
  }

  bool CButtonUI::Activate()
  {
    if (!CControlUI::Activate()) return false;
    if (m_pManager != NULL) m_pManager->SendNotify(this, DUI_MSGTYPE_CLICK);
    return true;
  }

  void CButtonUI::SetEnabled(bool bEnable)
  {
    CControlUI::SetEnabled(bEnable);
    if (!IsEnabled()) {
      m_uButtonState = 0;
    }
  }

  void CButtonUI::SetHotBkColor(DWORD dwColor)
  {
    m_dwHotBkColor = dwColor;
  }

  DWORD CButtonUI::GetHotBkColor() const
  {
    return m_dwHotBkColor;
  }

  void CButtonUI::SetHotTextColor(DWORD dwColor)
  {
    m_dwHotTextColor = dwColor;
  }

  DWORD CButtonUI::GetHotTextColor() const
  {
    return m_dwHotTextColor;
  }

  void CButtonUI::SetPushedTextColor(DWORD dwColor)
  {
    m_dwPushedTextColor = dwColor;
  }

  DWORD CButtonUI::GetPushedTextColor() const
  {
    return m_dwPushedTextColor;
  }

  void CButtonUI::SetFocusedTextColor(DWORD dwColor)
  {
    m_dwFocusedTextColor = dwColor;
  }

  DWORD CButtonUI::GetFocusedTextColor() const
  {
    return m_dwFocusedTextColor;
  }

  LPCTSTR CButtonUI::GetNormalImage()
  {
    return m_diNormal.sDrawString;
  }

  void CButtonUI::SetNormalImage(LPCTSTR pStrImage)
  {
    if (m_diNormal.sDrawString == pStrImage && m_diNormal.pImageInfo != NULL) return;
    m_diNormal.Clear();
    m_diNormal.sDrawString = pStrImage;
    m_gifHelperNormal->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetHotImage()
  {
    return m_diHot.sDrawString;
  }

  void CButtonUI::SetHotImage(LPCTSTR pStrImage)
  {
    if (m_diHot.sDrawString == pStrImage && m_diHot.pImageInfo != NULL) return;
    m_diHot.Clear();
    m_diHot.sDrawString = pStrImage;
    m_gifHelperHot->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetPushedImage()
  {
    return m_diPushed.sDrawString;
  }

  void CButtonUI::SetPushedImage(LPCTSTR pStrImage)
  {
    if (m_diPushed.sDrawString == pStrImage && m_diPushed.pImageInfo != NULL) return;
    m_diPushed.Clear();
    m_diPushed.sDrawString = pStrImage;
    m_gifHelperPushed->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetFocusedImage()
  {
    return m_diFocused.sDrawString;
  }

  void CButtonUI::SetFocusedImage(LPCTSTR pStrImage)
  {
    if (m_diFocused.sDrawString == pStrImage && m_diFocused.pImageInfo != NULL) return;
    m_diFocused.Clear();
    m_diFocused.sDrawString = pStrImage;
    m_gifHelperFocused->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetDisabledImage()
  {
    return m_diDisabled.sDrawString;
  }

  void CButtonUI::SetDisabledImage(LPCTSTR pStrImage)
  {
    if (m_diDisabled.sDrawString == pStrImage && m_diDisabled.pImageInfo != NULL) return;
    m_diDisabled.Clear();
    m_diDisabled.sDrawString = pStrImage;
    m_gifHelperDisabled->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetForeImage()
  {
    return m_diFore.sDrawString;
  }

  void CButtonUI::SetForeImage(LPCTSTR pStrImage)
  {
    if (m_diFore.sDrawString == pStrImage && m_diFore.pImageInfo != NULL) return;
    m_diFore.Clear();
    m_diFore.sDrawString = pStrImage;
    m_gifHelperFore->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetHotForeImage()
  {
    return m_diHotFore.sDrawString;
  }

  void CButtonUI::SetHotForeImage(LPCTSTR pStrImage)
  {
    if (m_diHotFore.sDrawString == pStrImage && m_diHotFore.pImageInfo != NULL) return;
    m_diHotFore.Clear();
    m_diHotFore.sDrawString = pStrImage;
    m_gifHelperHotFore->ReloadFile(pStrImage);
    Invalidate();
  }

  LPCTSTR CButtonUI::GetPushedForeImage()
  {
    return m_diPushedFore.sDrawString;
  }

  void CButtonUI::SetPushedForeImage(LPCTSTR pStrImage)
  {
    if (m_diPushedFore.sDrawString == pStrImage && m_diPushedFore.pImageInfo != NULL) return;
    m_diPushedFore.Clear();
    m_diPushedFore.sDrawString = pStrImage;
    m_gifHelperPushedFore->ReloadFile(pStrImage);
    Invalidate();
  }

  void CButtonUI::SetFiveStatusImage(LPCTSTR pStrImage)
  {
    m_diNormal.Clear();
    m_diNormal.sDrawString = pStrImage;
    DrawImage(NULL, m_diNormal);
    if (m_diNormal.pImageInfo) {
      LONG width = m_diNormal.pImageInfo->nX / 5;
      LONG height = m_diNormal.pImageInfo->nY;
      m_diNormal.rcBmpPart = CDuiRect(0, 0, width, height);
      if (m_bFloat && m_cxyFixed.cx == 0 && m_cxyFixed.cy == 0) {
        m_cxyFixed.cx = width;
        m_cxyFixed.cy = height;
      }
    }

    m_diPushed.Clear();
    m_diPushed.sDrawString = pStrImage;
    DrawImage(NULL, m_diPushed);
    if (m_diPushed.pImageInfo) {
      LONG width = m_diPushed.pImageInfo->nX / 5;
      LONG height = m_diPushed.pImageInfo->nY;
      m_diPushed.rcBmpPart = CDuiRect(width, 0, width * 2, height);
    }

    m_diHot.Clear();
    m_diHot.sDrawString = pStrImage;
    DrawImage(NULL, m_diHot);
    if (m_diHot.pImageInfo) {
      LONG width = m_diHot.pImageInfo->nX / 5;
      LONG height = m_diHot.pImageInfo->nY;
      m_diHot.rcBmpPart = CDuiRect(width * 2, 0, width * 3, height);
    }

    m_diFocused.Clear();
    m_diFocused.sDrawString = pStrImage;
    DrawImage(NULL, m_diFocused);
    if (m_diFocused.pImageInfo) {
      LONG width = m_diFocused.pImageInfo->nX / 5;
      LONG height = m_diFocused.pImageInfo->nY;
      m_diFocused.rcBmpPart = CDuiRect(width * 3, 0, width * 4, height);
    }

    m_diDisabled.Clear();
    m_diDisabled.sDrawString = pStrImage;
    DrawImage(NULL, m_diDisabled);
    if (m_diDisabled.pImageInfo) {
      LONG width = m_diDisabled.pImageInfo->nX / 5;
      LONG height = m_diDisabled.pImageInfo->nY;
      m_diDisabled.rcBmpPart = CDuiRect(width * 4, 0, width * 5, height);
    }

    Invalidate();
  }

  void CButtonUI::SetFadeAlphaDelta(BYTE uDelta)
  {
    m_uFadeAlphaDelta = uDelta;
  }

  BYTE CButtonUI::GetFadeAlphaDelta()
  {
    return m_uFadeAlphaDelta;
  }

  SIZE CButtonUI::EstimateSize(SIZE szAvailable)
  {
    if (m_cxyFixed.cy == 0) return CDuiSize(m_cxyFixed.cx, m_pManager->GetFontInfo(GetFont())->tm.tmHeight + 8);
    return CControlUI::EstimateSize(szAvailable);
  }

  void CButtonUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
  {
    if (_tcscmp(pstrName, _T("normalimage")) == 0) SetNormalImage(pstrValue);
    else if (_tcscmp(pstrName, _T("hotimage")) == 0) SetHotImage(pstrValue);
    else if (_tcscmp(pstrName, _T("pushedimage")) == 0) SetPushedImage(pstrValue);
    else if (_tcscmp(pstrName, _T("focusedimage")) == 0) SetFocusedImage(pstrValue);
    else if (_tcscmp(pstrName, _T("disabledimage")) == 0) SetDisabledImage(pstrValue);
    else if (_tcscmp(pstrName, _T("foreimage")) == 0) SetForeImage(pstrValue);
    else if (_tcscmp(pstrName, _T("hotforeimage")) == 0) SetHotForeImage(pstrValue);
    else if (_tcscmp(pstrName, _T("pushedforeimage")) == 0) SetPushedForeImage(pstrValue);
    else if (_tcscmp(pstrName, _T("fivestatusimage")) == 0) SetFiveStatusImage(pstrValue);
    else if (_tcscmp(pstrName, _T("fadedelta")) == 0) SetFadeAlphaDelta((BYTE)_ttoi(pstrValue));
    else if (_tcscmp(pstrName, _T("hotbkcolor")) == 0)
    {
      if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
      LPTSTR pstr = NULL;
      DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
      SetHotBkColor(clrColor);
    } else if (_tcscmp(pstrName, _T("hottextcolor")) == 0)
    {
      if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
      LPTSTR pstr = NULL;
      DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
      SetHotTextColor(clrColor);
    } else if (_tcscmp(pstrName, _T("pushedtextcolor")) == 0)
    {
      if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
      LPTSTR pstr = NULL;
      DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
      SetPushedTextColor(clrColor);
    } else if (_tcscmp(pstrName, _T("focusedtextcolor")) == 0)
    {
      if (*pstrValue == _T('#')) pstrValue = ::CharNext(pstrValue);
      LPTSTR pstr = NULL;
      DWORD clrColor = _tcstoul(pstrValue, &pstr, 16);
      SetFocusedTextColor(clrColor);
    } else CLabelUI::SetAttribute(pstrName, pstrValue);
  }

  void CButtonUI::PaintText(HDC hDC)
  {
    if (IsFocused()) m_uButtonState |= UISTATE_FOCUSED;
    else m_uButtonState &= ~UISTATE_FOCUSED;
    if (!IsEnabled()) m_uButtonState |= UISTATE_DISABLED;
    else m_uButtonState &= ~UISTATE_DISABLED;

    if (m_dwTextColor == 0) m_dwTextColor = m_pManager->GetDefaultFontColor();
    if (m_dwDisabledTextColor == 0) m_dwDisabledTextColor = m_pManager->GetDefaultDisabledColor();

    if (m_sText.IsEmpty()) return;
    int nLinks = 0;
    RECT rc = m_rcItem;
    rc.left += m_rcTextPadding.left;
    rc.right -= m_rcTextPadding.right;
    rc.top += m_rcTextPadding.top;
    rc.bottom -= m_rcTextPadding.bottom;

    DWORD clrColor = IsEnabled() ? m_dwTextColor : m_dwDisabledTextColor;

    if (((m_uButtonState & UISTATE_PUSHED) != 0) && (GetPushedTextColor() != 0))
      clrColor = GetPushedTextColor();
    else if (((m_uButtonState & UISTATE_HOT) != 0) && (GetHotTextColor() != 0))
      clrColor = GetHotTextColor();
    else if (((m_uButtonState & UISTATE_FOCUSED) != 0) && (GetFocusedTextColor() != 0))
      clrColor = GetFocusedTextColor();

    if (m_bShowHtml)
      CRenderEngine::DrawHtmlText(hDC, m_pManager, rc, m_sText, clrColor, \
        NULL, NULL, nLinks, m_iFont, m_uTextStyle);
    else
      CRenderEngine::DrawText(hDC, m_pManager, rc, m_sText, clrColor, \
        m_iFont, m_uTextStyle);
  }

  void CButtonUI::PaintStatusImage(HDC hDC)
  {
    if (IsFocused()) m_uButtonState |= UISTATE_FOCUSED;
    else m_uButtonState &= ~UISTATE_FOCUSED;
    if (!IsEnabled()) m_uButtonState |= UISTATE_DISABLED;
    else m_uButtonState &= ~UISTATE_DISABLED;

    if ((m_uButtonState & UISTATE_DISABLED) != 0) {
      if (/*DrawImage(hDC, m_diDisabled)*/m_gifHelperDisabled->Draw(hDC, this)) goto Label_ForeImage;
    } else if ((m_uButtonState & UISTATE_PUSHED) != 0) {
      if (!/*DrawImage(hDC, m_diPushed)*/m_gifHelperPushed->Draw(hDC, this))
        //DrawImage(hDC, m_diNormal);
        m_gifHelperNormal->Draw(hDC, this);
      if (/*DrawImage(hDC, m_diPushedFore)*/m_gifHelperPushedFore->Draw(hDC, this)) return;
      else goto Label_ForeImage;
    } else if ((m_uButtonState & UISTATE_HOT) != 0) {
      if (GetFadeAlphaDelta() > 0) {
        if (m_uFadeAlpha == 0) {
          m_diHot.uFade = 255;
          /*DrawImage(hDC, m_diHot);*/
          m_gifHelperHot->Draw(hDC, this);
        } else {
          //m_diNormal.uFade = m_uFadeAlpha;
          //DrawImage(hDC, m_diNormal);
          m_gifHelperNormal->SetStaticImageFade(m_uFadeAlpha);
          m_gifHelperNormal->Draw(hDC, this);
          m_diHot.uFade = 255 - m_uFadeAlpha;
          /*DrawImage(hDC, m_diHot);*/
          m_gifHelperHot->Draw(hDC, this);
        }
      } else {
        if (!/*DrawImage(hDC, m_diHot)*/m_gifHelperHot->Draw(hDC, this))
          //DrawImage(hDC, m_diNormal);
          m_gifHelperNormal->Draw(hDC, this);
      }

      if (/*DrawImage(hDC, m_diHotFore)*/ m_gifHelperHotFore->Draw(hDC, this)) return;
      else if (m_dwHotBkColor != 0) {
        CRenderEngine::DrawColor(hDC, m_rcPaint, GetAdjustColor(m_dwHotBkColor));
        return;
      } else goto Label_ForeImage;
    } else if ((m_uButtonState & UISTATE_FOCUSED) != 0) {
      if (/*DrawImage(hDC, m_diFocused)*/m_gifHelperFocused->Draw(hDC, this)) goto Label_ForeImage;;
    }

    if (GetFadeAlphaDelta() > 0) {
      if (m_uFadeAlpha == 255) {
        //m_diNormal.uFade = 255;
        //DrawImage(hDC, m_diNormal);
        m_gifHelperNormal->SetStaticImageFade(255);
        m_gifHelperNormal->Draw(hDC, this);
      } else {
        m_diHot.uFade = 255 - m_uFadeAlpha;
        /*DrawImage(hDC, m_diHot);*/
        m_gifHelperHot->Draw(hDC, this);
        //m_diNormal.uFade = m_uFadeAlpha;
        //DrawImage(hDC, m_diNormal);
        m_gifHelperNormal->SetStaticImageFade(m_uFadeAlpha);
        m_gifHelperNormal->Draw(hDC, this);
      }
    } else {
      //DrawImage(hDC, m_diNormal);
      m_gifHelperNormal->Draw(hDC, this);
    }

Label_ForeImage:
    /*DrawImage(hDC, m_diFore);*/
    m_gifHelperFore->Draw(hDC, this);
  }
}