#pragma once
#include <codecvt>

#ifdef RapidJsonConvenient
namespace rapidjson
{
  template<class R, class T>
  inline bool IsType(const T& d, char const* field)
  {
    auto it = d.FindMember(field);
    return (it != d.MemberEnd() && it->value.Is<R>());
  }
  template<class R, class T>
  inline R GetValue(T const& d, char const*field, R def)
  {
    auto it = d.FindMember(field);
    return IsType<R>(d, field) ? it->value.Get<R>() : def;
  }
  template<class T>
  inline StringBuffer GetStringBuf(T const&d)
  {
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);
    d.Accept(writer);
    return buf;
  }

  /*
   *  ������������
   */
  template<class N, class A, class ...Args>
  N&& AddMembers(N&& d, A&, Args&&...) { return std::forward<N>(d); }
  template<class N, class A, class I, class V, class ...Args>
  N&& AddMembers(N&& d, A &a, I&&i, V&&v, Args&&...args)
  {
    d.AddMember(std::forward<I>(i), std::forward<V>(v), a);
    AddMembers(d, a, std::forward<Args>(args)...);
    return std::forward<N>(d);
  }
}
#endif // RapidJsonConvenient

//#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
//inline std::wstring UTF8_2W(char const*utf8_data)
//{
//  return std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(utf8_data);
//}
