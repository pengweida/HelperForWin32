#pragma once

#pragma warning(push)
#pragma warning(disable:4003)
#include "document.h"
#include "istreamwrapper.h"
#include "pointer.h"
#include "stringbuffer.h"
#include "writer.h"
#pragma warning(pop)
using namespace rapidjson;
using DocumentPtr = std::shared_ptr<Document>;
#define MakeDocObj(val,a) DocumentPtr val = make_shared<Document>(kObjectType);rapidjson::Document::AllocatorType& a = val->GetAllocator()

namespace rapidjson
{
  template<class R, class T>
  bool IsType(const T& d, const char* field) {
    auto it = d.FindMember(field);
    return (it != d.MemberEnd() && it->value.Is<R>());
  }

  template<class R, class T>
  R GetValue(const T& d, const char* field, R def) {
    auto it = d.FindMember(field);
    return (it != d.MemberEnd() && it->value.Is<R>()) ? it->value.Get<R>() : def;
  }

  //! 获取StringBuffer
  /*!
  \tparam d Document 或 Value类型
  \return StringBuffer对象
  */
  template<class T>
  inline StringBuffer GetStringBuf(const T& d) {
    StringBuffer buf;
    Writer<StringBuffer> writer(buf);
    d.Accept(writer);
    return buf;
  }

  template<typename V>
  std::string GetDocStr(V&&v)
  {
    return GetStringBuf(v).GetString();
  }

  //! 批量添加成员
/*!
\tparam n Document 或 Value类型
\tparam a Allocator
\tparam i IndexType，兼容所有规定类型
\tparam v ValueType，兼容所有规定类型
\return 流畅接口定义，将n直接返回，\see code
\code
rapidjson::Document d(rapidjson::kObjectType);
auto& a = d.GetAllocator();
rapidjson::AddMembers(d, a,
"a", 1,
"b", false,
"c", rapidjson::AddMembers(rapidjson::Value(rapidjson::kObjectType), a,
"d", true)
);
std::cout << rapidjson::GetStringBuf(d).GetString();
\endcode
*/
  template<class N, class A>
  N& AddMembers(N& n, A& a) { return n; }
  template<class N, class A, class I, class V, class... Args>
  N& AddMembers(N& n, A& a, I&& i, V&& v, Args&&... args)
  {
    n.AddMember(std::forward<I>(i), std::forward<V>(v), a);
    AddMembers(n, a, std::forward<Args>(args)...);
    return n;
  }


  template<typename V1, typename V2, typename Alloc>
  void ExtractDoc(V1 &&source, V2 &&dest, Alloc &&alloc, std::set<std::string> keywords, bool is_include = true)
  {
    if (is_include) {
      std::for_each(keywords.begin(), keywords.end(), [&](auto const&keyword) {
        if (dest.HasMember(keyword.c_str()))dest.RemoveMember(keyword.c_str());
        dest.AddMember(Value{ StringRef(keyword.c_str()),alloc }, Value{}, alloc);
        dest[keyword.c_str()].CopyFrom(source[keyword.c_str()], alloc, true);
      });
    } else {
      for (auto&v : source.GetObject()) {
        if (keywords.count(v.name.GetString()) == 0) {
          if (dest.HasMember(v.name.GetString()))dest.RemoveMember(v.name.GetString());
          dest.AddMember(Value{ StringRef(v.name.GetString()),alloc }, Value{}, alloc);
          dest[v.name.GetString()].CopyFrom(v.value, alloc, true);
        }
      }
    }
  }
}