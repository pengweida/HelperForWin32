#pragma once

class $CLASSNAME$
  :public WindowImpl
{

  DUI_DECLARE_WND_CLASS(L"HelperUI", CS_DBLCLKS, L"$SKINFILENAME$")
public:
  $CLASSNAME$();

protected:

  virtual void OnInitWnd() override;

  virtual void Notify(TNotifyUI& msg) override;

  virtual LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override;

private:
$MEMBERSECTION$
};