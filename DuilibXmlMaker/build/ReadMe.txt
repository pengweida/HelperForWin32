在创建节点时

F2    	重复上次创建
F3	在当前选中节点下创建子节点
回车	保存并查看xml效果


这些宏可以在uipatch.h文件中找到

代码生成几个宏的说明
#define DUI_DECLARE_WND_CLASS(WndClassName, WndClassStyle, szXmlFileName) \
	LPCTSTR GetWindowClassName() const override { return WndClassName; } \
  UINT GetClassStyle() const override { return WndClassStyle; } \
  CDuiString GetSkinFile() override { return (LPCTSTR)szXmlFileName; } \
  CDuiString GetSkinFolder() override { return _T(""); }


#define BEGIN_DUICONTROL_CREATE(theClass) \
	DuiLib::CControlUI* CreateControl(LPCTSTR pstrClass) \
	{ \
		DuiLib::CDuiString strClass(pstrClass);

#define DUICONTROL_CREATE(XmlTagName, UIClass) \
	if ( ! strClass.CompareNoCase(XmlTagName) ) \
		return ( new UIClass );

#define DUICONTROL_CREATE_FROM_XML(XmlTagName, XmlFile) \
    if ( ! strClass.CompareNoCase(XmlTagName) ) \
    { \
        DuiLib::CDialogBuilder builder; \
        DuiLib::CControlUI *pControl = builder.Create(XmlFile, NULL, this, &m_PaintManager); \
        ASSERT(pControl != NULL && "DUICONTROL_CREATE_FROM_XML"); \
        return pControl; \
    }

#define END_DUICONTROL_CREATE() \
		return NULL; \
	}



  template<typename N, typename V>
  bool FindControl(const N& clue, V*& control)
  {
    control = static_cast<V *>(__PM().FindControl(clue));
    return control != nullptr;
  }



  // 统一性原则，重命名InitWindow为OnInitWnd
  virtual void InitWindow() override final { OnInitWnd(); }
