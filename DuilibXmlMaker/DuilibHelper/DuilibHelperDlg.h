﻿
// DuilibHelperDlg.h: 头文件
//

#pragma once

// CDuilibHelperDlg 对话框
class CDuilibHelperDlg : public CDialogEx
{
  // 构造
public:
  CDuilibHelperDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
  enum { IDD = IDD_DUILIBHELPER_DIALOG };
#endif

protected:
  virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
  HICON m_hIcon;

  // 生成的消息映射函数
  virtual BOOL OnInitDialog();
  afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
  afx_msg void OnPaint();
  afx_msg HCURSOR OnQueryDragIcon();
  DECLARE_MESSAGE_MAP()

private:
  void SetupUI();
  void LoadAttrFile();

  afx_msg void OnBnClickedButtonNew();
  afx_msg void OnBnClickedButtonOpen();
  afx_msg void OnAttrNameListSelChanged();

  // 查看信息
  bool SetSel(wstring ctrl_name, wstring attr_name);

  int nSelItem; //列表丢失焦点不隐藏选中
  afx_msg void OnPropertyListSetFocus(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnPropertyListKillFocus(NMHDR* pNMHDR, LRESULT* pResult);

  CListBox name_list;
  CListCtrl desc_list;

};
