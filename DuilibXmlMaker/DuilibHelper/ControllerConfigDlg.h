﻿#pragma once


// CControllerConfigDlg 对话框

class CControllerConfigDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CControllerConfigDlg)

public:
	CControllerConfigDlg(CString controller_name,CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CControllerConfigDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_PIC_CONFIG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

  virtual BOOL OnInitDialog();

  virtual void OnOK();
private:
  CString controller_name_;

  afx_msg void OnSelChangedLeft();
  afx_msg void OnSelChangedRight();

  CListBox m_list_remain;
  CListBox m_list_sel;
  BOOL is_vertical_stretch_{true};
  BOOL is_quick_scale_{false};
  CString pic_res_path_{LR"(res\)"};
  int pic_offset_{0};
  int pic_width_{0};
  int pic_height_{0};
};
