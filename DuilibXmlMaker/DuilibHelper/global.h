#pragma once

class Global
  :public helper_lib::SingletonT<Global>
{
  HELPERLIBSINGLETON(Global);

public:
  bool Init(LPCWSTR attr_list_file_path)
  {

    attr_list_file_path_ = attr_list_file_path;
    auto parse_result = doc_.load_file(attr_list_file_path);
    if (parse_result.status != status_ok) {
      return false;
    }

    xml_node root = doc_.child(L"Controls");
    if (root.empty())return false;
    for (xml_node node = root.first_child(); node; node = node.next_sibling()) {
      AttrMap attr_mp;
      LoadAttr(attr_mp, root, node.name());
      controller_attr_mp_[node.name()] = std::move(attr_mp);
    }
    return true;
  }

  void UpdateConfigData(ControllerConfigData const&data)
  {
    config_data_mp_[data.ctr_name.GetString()] = data;
  }

  bool SetXmlNodeContentByConfigData(xml_node node,CString controller_name,bool next = false)
  {
    last_set_controller_name_ = controller_name;
    if (config_data_mp_.find(controller_name.GetString()) != config_data_mp_.end()) {
      ControllerConfigData data;
      if (next)data = config_data_mp_[controller_name.GetString()].Next();
      else data = config_data_mp_[controller_name.GetString()];

      data.ImgAttrForEach([&](CString key, CString value) {
        node.remove_attribute(key.GetString());
        node.append_attribute(key.GetString()) = value.GetString();
      });

      node.remove_attribute(L"width");
      node.append_attribute(L"width") = std::to_wstring(data.pic_width).c_str();

      node.remove_attribute(L"height");
      node.append_attribute(L"height") = std::to_wstring(data.pic_height).c_str();

      return true;
    }

    return false;
  }

  ControllerAttrMap controller_attr_mp_{};

  ConfigDataMap config_data_mp_{};

  CString last_set_controller_name_{};
private:

  void LoadAttr(AttrMap& attr_mp, xml_node const&root, wstring control_name)
  {
    if (controller_attr_mp_.find(control_name) != controller_attr_mp_.end()) {
      auto &mp = controller_attr_mp_[control_name];
      for (auto p : mp) attr_mp[p.first] = p.second;
      return;
    }

    auto controller_node = root.child(control_name.c_str());
    if (controller_node.empty())return;
    for (auto attr_node : controller_node.children()) {
      if (attr_node.type() != node_element)break;
      wstring attr_name = attr_node.attribute(L"name").as_string();
      //���ؼ����ݸ����ӿؼ�����
      attr_mp[attr_name] = Attribute{
        control_name,
        attr_name,
        attr_node.attribute(L"default").as_string(),
        attr_node.attribute(L"type").as_string(),
        attr_node.attribute(L"comment").as_string()
      };
    }

    wstring parent_control_name = controller_node.attribute(L"parent").as_string();
    if (!parent_control_name.empty()) {
      LoadAttr(attr_mp, root, parent_control_name);
    }
  }

private:
  xml_document doc_;
  std::wstring attr_list_file_path_;

};


#define GLOBAL() Global::GetInstance()