﻿// CControlChoiceDlg.cpp: 实现文件
//

#include "stdafx.h"
#include "DuilibHelper.h"
#include "ControlChoiceDlg.h"
#include "afxdialogex.h"


// CControlChoiceDlg 对话框

IMPLEMENT_DYNAMIC(CControlChoiceDlg, CDialogEx)

CControlChoiceDlg::CControlChoiceDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_CHOICECTRL, pParent)
{

}

CControlChoiceDlg::~CControlChoiceDlg()
{
}

void CControlChoiceDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialogEx::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_COMBO1, ctrl_combo_);
}


BEGIN_MESSAGE_MAP(CControlChoiceDlg, CDialogEx)
  ON_BN_CLICKED(IDC_BUTTON_OK, &CControlChoiceDlg::OnBnClickedButtonOk)
END_MESSAGE_MAP()


// CControlChoiceDlg 消息处理程序


void CControlChoiceDlg::OnBnClickedButtonOk()
{
  ctrl_combo_.GetWindowText(choice_ctrl_name);
  OnOK();
}


BOOL CControlChoiceDlg::OnInitDialog()
{
  CDialogEx::OnInitDialog();

  for (auto tmp : GLOBAL()->controller_attr_mp_) {
    ctrl_combo_.AddString(tmp.first.c_str());
  }

  ctrl_combo_.SetCurSel(0);

  return TRUE;
}
