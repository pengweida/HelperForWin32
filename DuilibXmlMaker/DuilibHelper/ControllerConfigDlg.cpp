﻿// ControllerConfigDlg.cpp: 实现文件
//

#include "stdafx.h"
#include "DuilibHelper.h"
#include "ControllerConfigDlg.h"
#include "afxdialogex.h"


// CControllerConfigDlg 对话框

IMPLEMENT_DYNAMIC(CControllerConfigDlg, CDialogEx)

CControllerConfigDlg::CControllerConfigDlg(CString controller_name, CWnd* pParent /*=nullptr*/)
  : CDialogEx(IDD_DIALOG_PIC_CONFIG, pParent), controller_name_(controller_name)
  , is_vertical_stretch_(FALSE)
  , is_quick_scale_(FALSE)
  , pic_res_path_(_T(""))
  , pic_offset_(0)
  , pic_width_(0)
  , pic_height_(0)
{

}

CControllerConfigDlg::~CControllerConfigDlg()
{
}

void CControllerConfigDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialogEx::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_1, m_list_remain);
  DDX_Control(pDX, IDC_LIST_2, m_list_sel);
  DDX_Check(pDX, IDC_CHECK1, is_vertical_stretch_);
  DDX_Check(pDX, IDC_CHECK2, is_quick_scale_);
  DDX_Text(pDX, IDC_EDIT1, pic_res_path_);
  DDX_Text(pDX, IDC_EDIT2, pic_offset_);
  DDX_Text(pDX, IDC_EDIT3, pic_width_);
  DDX_Text(pDX, IDC_EDIT4, pic_height_);
}


BEGIN_MESSAGE_MAP(CControllerConfigDlg, CDialogEx)
  ON_LBN_SELCHANGE(IDC_LIST_1, &CControllerConfigDlg::OnSelChangedLeft)
  ON_LBN_SELCHANGE(IDC_LIST_2, &CControllerConfigDlg::OnSelChangedRight)
END_MESSAGE_MAP()


// CControllerConfigDlg 消息处理程序


void CControllerConfigDlg::OnSelChangedLeft()
{
  int index = m_list_remain.GetCurSel();
  if (index < 0)
    return;
  CString attr_add;
  m_list_remain.GetText(index, attr_add);
  m_list_sel.AddString(attr_add);
  m_list_remain.DeleteString(index);
}


void CControllerConfigDlg::OnSelChangedRight()
{
  int index = m_list_sel.GetCurSel();
  if (index < 0)
    return;
  CString attr_delete;
  m_list_sel.GetText(index, attr_delete);
  m_list_remain.AddString(attr_delete);
  m_list_sel.DeleteString(index);
}


BOOL CControllerConfigDlg::OnInitDialog()
{
  CDialogEx::OnInitDialog();
  is_vertical_stretch_ = true;
  pic_res_path_ = LR"(res\)";
  UpdateData(FALSE);

  SetWindowText(controller_name_ + L"背景图设置向导");

  for (auto tmp : GLOBAL()->controller_attr_mp_[controller_name_.GetBuffer()]) {
    if (tmp.first.find(L"image") != string::npos) {  //包含image的属性
      m_list_remain.AddString(tmp.first.c_str());
    }
  }

  auto &config_data_mp = GLOBAL()->config_data_mp_;
  if (config_data_mp.find(controller_name_.GetBuffer()) != config_data_mp.end()) {
    auto &data = config_data_mp[controller_name_.GetString()].Next();
    pic_res_path_ = data.pic_name;
    pic_width_ = data.pic_width;
    pic_height_ = data.pic_height;
    is_vertical_stretch_ = data.is_longitudinal;
    is_quick_scale_ = data.is_zoom;
    pic_offset_ = data.start_pos;
    UpdateData(FALSE);
    for (auto it : data.attrs) {
      int index = m_list_remain.FindStringExact(0, it.GetBuffer());
      if (index < 0)continue;
      m_list_remain.DeleteString(index);
      m_list_sel.AddString(it.GetBuffer());
    }
  }

  return TRUE;  // return TRUE unless you set the focus to a control
                // 异常: OCX 属性页应返回 FALSE
}

void CControllerConfigDlg::OnOK()
{
  UpdateData(TRUE);
  ControllerConfigData data;
  data.is_longitudinal = is_vertical_stretch_;
  data.is_zoom = is_quick_scale_;
  data.pic_name = pic_res_path_;
  data.pic_width = pic_width_;
  data.pic_height = pic_height_;
  data.start_pos = pic_offset_;
  CString attr;
  for (int i = 0; i < m_list_sel.GetCount(); i++) {
    m_list_sel.GetText(i, attr);
    data.attrs.push_back(attr);
  }
  data.ctr_name = controller_name_;
  GLOBAL()->UpdateConfigData(data);

  return __super::OnOK();
}
