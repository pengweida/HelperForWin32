#pragma once

#define USE(Type) Application::Instance()->Get##Type()
#define DEFINE(Type, name) Type* name = USE(Type);


// 窗口消息
enum MessageType
{
  kAsyncTask = WM_APP + 100,
};

// 异步任务
class AsyncTask
{
protected:
  typedef std::function<void(void)> Task;

public:
  AsyncTask(Task&& task) : task_(std::move(task)) {};
  AsyncTask(Task const& task) : task_(task) {};
  virtual ~AsyncTask() {};
  void Run() { if (task_) task_(); }
  static inline bool Post(HWND notify, AsyncTask* task)
  {
    bool ok = !!::PostMessage(notify, kAsyncTask, 0, LPARAM(task));
    if (!ok) {
      assert(false);
      delete task;
    }
    return ok;
  }

  static inline bool Post(Task const&task)
  {
    return Post(USE(MainFrameWnd), new AsyncTask(task));
  }
private:
  Task task_;
};
template<class T>
class AsyncTaskT
  : public AsyncTask
{
public:
  template<class _Type>
  AsyncTaskT(Task&& task, _Type&& data)
    : AsyncTask(std::forward<Task>(task))
    , data_(std::forward<_Type>(data))
  {
  }

private:
  std::shared_ptr<T> data_;
};

HANDLE Exec(HWND hWnd, LPCTSTR path, LPCTSTR args = nullptr, LPCTSTR dir = nullptr);