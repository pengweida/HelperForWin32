#pragma once

class MainFrame
  :public WindowImpl
{

  LPCTSTR GetWindowClassName() const override { return L"Previewer"; }
  UINT GetClassStyle() const override { return CS_DBLCLKS; }
  CDuiString GetSkinFile() override { return skin_file_name_; }
  CDuiString GetSkinFolder() override { return L""; }
public:
  MainFrame();

  void SetSkinFileName(LPCWSTR file_name);
protected:
  virtual void OnInitWnd() override;

  virtual void OnFinalMessage(HWND hWnd) override;

  virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) override;

  //全局消息过滤
  virtual LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, bool& bHandled) override;
protected:

  void AcceptDropFile();
private:
  CDuiString skin_file_name_{};
};

