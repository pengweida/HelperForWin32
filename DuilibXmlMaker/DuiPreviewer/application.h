#pragma once

class MainFrame;
class Application
{
  Application();
  ~Application();
public:

  static Application* Instance();

  static void DestroyInstance();

  bool Initialize(HINSTANCE inst);

  bool Run(LPCWSTR strCmdLine);

  void Uninitialize();


  inline DWORD GetMainThreadId() const;
  inline BOOL GetIsMainThread() const;

  const std::wstring& GetDataStorageDir() const;
  const std::wstring& GetModuleDir() const;

  MainFrame* GetMainFrame() { return main_frame_.get(); }
  HWND GetMainFrameWnd() const;

protected:

private:
  const DWORD main_thread_id_;

  CComModule module_;
  std::wstring module_dir_;
  std::wstring data_storage_directory_;

  std::unique_ptr<MainFrame> main_frame_;

  fs::path file_path_{};
};