#include "stdafx.h"
#include "typedef.h"

HANDLE Exec(HWND hWnd, LPCTSTR path, LPCTSTR args/* = nullptr*/, LPCTSTR dir/* = nullptr*/)
{
  if (!path || !::PathFileExists(path))
    return nullptr;

  SHELLEXECUTEINFO execute_info;
  execute_info.cbSize = sizeof(SHELLEXECUTEINFO);
  execute_info.fMask = SEE_MASK_NOCLOSEPROCESS;
  execute_info.hwnd = hWnd;
  execute_info.lpVerb = NULL;
  execute_info.lpFile = path;
  execute_info.lpParameters = args;
  if (dir != nullptr) {
    execute_info.lpDirectory = dir;
  } else {
    TString dir = path;
    dir = dir.substr(0, dir.rfind(L"\\") + 1);
    execute_info.lpDirectory = dir.c_str();
  }
  execute_info.nShow = SW_SHOWNORMAL;
  execute_info.hInstApp = NULL;

  return ::ShellExecuteEx(&execute_info) ? execute_info.hProcess : nullptr;
}