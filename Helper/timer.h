#pragma once
#include "common.h"
#include "binary_heap.h"
#include "condition_variable.h"
#include <functional>
#include <memory>
#include <chrono>
#include <algorithm>
#include <map>

HelperLibBegin

class timer_manager
{
  using Clock = ::std::chrono::high_resolution_clock;
  using Lock = unique_lock<mutex>;
public:
  using id = uint64_t;
  using timer_fn = ::std::function<bool(id)>;

  //用于timer被集成到其他消息过滤循环时,通知需要进行time tick
  void SetShouldCheckNotify(::std::function<void()> fn)
  {
    Lock lk{ mtx_ };
    notify_fn_ = fn;
  }

  id SetTimer(uint32_t elapse_milli, timer_fn fn, bool one_shot = false)
  {
    timer::ptr tm{ new timer };
    Lock lk{ mtx_ };
    {
      tm->id_ = GenNextId();
      tm->fn_ = fn;
      tm->one_shot_ = one_shot;
      tm->interval_ = ::std::chrono::milliseconds{ elapse_milli };
      tm->expire_time_ = Clock::now() + tm->interval_;

      timer_heap_.push(tm);
      timer_mp_[tm->id_] = tm;
      if (timer_heap_.front() == tm) {
        cv_.notify_all();
        if (notify_fn_)notify_fn_();
      }
    }
    return tm->id_;
  }

  void KillTimer(id timer_id)
  {
    Lock lk{ mtx_ };
    auto it_find = timer_mp_.find(timer_id);

    if (it_find != timer_mp_.end()) {
      it_find->second->is_delete_ = true;
    }
  }

  void Stop()
  {
    Lock lk{ mtx_ };
    is_stop_ = true;
    cv_.notify_all();
  }

  bool IsStop() const
  {
    Lock lk{ mtx_ };
    return is_stop_;
  }

  void TimerLoop()
  {
    Lock lk{ mtx_ };
    for (;;) {
      if (is_stop_) return;
      cv_.wait_for(lk, ::std::chrono::milliseconds{ Tick() });
    }
  }

  uint32_t Tick()
  {
    Lock lk{ mtx_ };
    while (!timer_heap_.empty()) {
      if (is_stop_) return INFINITE;
      auto now = Clock::now();
      auto tm = timer_heap_.front();
      if (tm->is_delete_) {
        timer_mp_.erase(tm->id_);
        timer_heap_.pop();
      } else if (tm->expire_time_ <= now) {
        bool should_delete = !tm->fn_;
        if (tm->fn_) {
          should_delete = !tm->fn_(tm->id_);
        }
        if (should_delete || tm->is_delete_ || tm->one_shot_) {
          timer_mp_.erase(tm->id_);
          timer_heap_.pop();
        } else {
          tm->expire_time_ = tm->expire_time_ + tm->interval_;
          timer_heap_.pop();
          timer_heap_.push(tm);
        }
      } else if (!timer_heap_.empty()) {
        return (uint32_t)::std::chrono::duration_cast<::std::chrono::milliseconds>(timer_heap_.front()->expire_time_ - now).count();
      }
    }

    return INFINITE;
  }

private:
  struct timer
  {
    using ptr = ::std::shared_ptr<timer>;

    friend bool operator<(timer::ptr const&left, timer::ptr const&right) { return left->expire_time_ < right->expire_time_; }

    id id_;
    timer_fn fn_;
    bool one_shot_{ false };
    bool is_delete_{ false };
    ::std::chrono::milliseconds interval_;
    Clock::time_point expire_time_{};
  };

  id GenNextId() { return id_seq_++; }

  binary_heap<timer::ptr> timer_heap_;
  ::std::map<id, timer::ptr> timer_mp_;

  mutable mutex mtx_;
  condition_variable cv_;
  id id_seq_ = 0;

  ::std::function<void()> notify_fn_{};

  bool is_stop_{ false };
};

HelperLibEnd





//helper_lib::timer_manager tm;
//
//std::thread{ [&] {tm.TimerLoop(); } }.detach();
//
//tm.SetTimer(1000, [&](auto const&id)
//{
//  MyLog << id;
//  return true;
//});
//
//tm.SetTimer(100, [&](auto const&id)
//{
//  MyLog << id;
//  return true;
//});
//
//Sleep(5000);
//tm.Stop();