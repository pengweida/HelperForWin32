#include "stdafx.h"
#include "application.h"
#include "main_frame.h"

int APIENTRY wWinMain(
  _In_ HINSTANCE ins,
  _In_opt_ HINSTANCE,
  _In_ LPWSTR    lpCmdLine,
  _In_ int       nCmdShow)
{
  UNREFERENCED_PARAMETER(lpCmdLine);
  UNREFERENCED_PARAMETER(nCmdShow);

  Application* app = Application::Instance();
  if (app) {
    if (app->Initialize(ins)) {
      app->Run(lpCmdLine);
      app->Uninitialize();
    }
    app->DestroyInstance();
    app = nullptr;
  }

  return 0;
}

namespace
{
  Application *g_application_ = nullptr;
}

Application::Application()
  :main_thread_id_(::GetCurrentThreadId())
{

}

Application::~Application()
{

}

Application* Application::Instance()
{
  return g_application_ ? g_application_ : (g_application_ = new(std::nothrow) Application);
}

void Application::DestroyInstance()
{
  if (g_application_) delete g_application_;
}

bool Application::Initialize(HINSTANCE inst)
{
  // 更改当前控制台代码页为UTF8解决乱码问题
  ::SetConsoleOutputCP(65001);

  // Duilib
  CPaintManagerUI::SetInstance(inst);
  CPaintManagerUI::SetResourcePath(CPaintManagerUI::GetInstancePath() + L"skin\\");
  CPaintManagerUI::SetCurrentPath(CPaintManagerUI::GetInstancePath().GetData());

  // COM
  HRESULT hr = ::CoInitializeEx(NULL,
#if defined(WIN32) && !defined(UNDER_CE)
    COINIT_APARTMENTTHREADED
#else
    COINIT_MULTITHREADED
#endif
  );
  if (FAILED(hr)) return false;

  // this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
  ::DefWindowProc(NULL, 0, 0, 0L);

  // Ole
  if (FAILED(::OleInitialize(NULL))) return false;

  // ATL
  if (FAILED(module_.Init(NULL, inst))) return false;

  // Locale
  std::locale::global(std::locale(""));

  // CWndShadow
  CWndShadow::Initialize(inst);

  WCHAR module_dir[MAX_PATH]{};
  module_dir_ = GetCurModuleDir(module_dir, MAX_PATH);
  namespace fs = std::experimental::filesystem;
  data_storage_directory_ = fs::canonical(fs::path{ module_dir } / _T("profile\\"));

  Profile profile{ (fs::path{data_storage_directory_} / L"profile.xml").c_str() };

  profile.Set(L"name", L"zhangsan");
  //TODO other init

  return true;
}

bool Application::Run(LPCWSTR strCmdLine)
{
  int num_args = 0;
  LPWSTR *szArgList = ::CommandLineToArgvW(::GetCommandLineW(), &num_args);

  //创建窗口
  main_frame_.reset(new MainFrame);
  main_frame_->Create(NULL, _T("DUIWnd"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
  main_frame_->CenterWindow();
  main_frame_->ShowWindow();


  CPaintManagerUI::MessageLoop();
  CPaintManagerUI::Term();

  return true;
}

void Application::Uninitialize()
{
  module_.Term();
  ::OleUninitialize();
  ::CoUninitialize();
}

DWORD Application::GetMainThreadId() const
{
  return main_thread_id_;
}

BOOL Application::GetIsMainThread() const
{
  return main_thread_id_ == ::GetCurrentThreadId();
}

MainFrame* Application::GetMainFrame() const
{
  return main_frame_.get();
}

const std::wstring& Application::GetDataStorageDir() const
{
  return data_storage_directory_;
}

const std::wstring& Application::GetModuleDir() const
{
  return module_dir_;
}
