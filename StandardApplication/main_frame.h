#pragma once

class MainFrame
  :public WindowImpl
{

  DUI_DECLARE_WND_CLASS(L"DUIMainFrame", CS_DBLCLKS, L"a.xml")
public:
  MainFrame();

protected:

  virtual void OnInitWnd() override;

  virtual void OnFinalMessage(HWND hWnd) override;

  virtual void Notify(TNotifyUI& msg) override;

  virtual LRESULT HandleCustomMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override;

private:

};