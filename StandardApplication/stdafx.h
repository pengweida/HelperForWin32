﻿#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 从 Windows 头文件中排除极少使用的内容
// Windows 头文件
#include <windows.h>
#include <atlbase.h>
#include <tlhelp32.h>
#include <shellapi.h>

// C 运行时头文件
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// C++ 头文件
#include <chrono>
#include <algorithm>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <memory>
#include <xlocale>
#include <functional>
#include <sstream>
#include <filesystem>
#include <map>
#include <queue>
#include <array>
#include <set>
#include <unordered_map>
#include <tuple>
namespace fs = std::experimental::filesystem;
#ifdef _UNICODE
typedef std::wstring TString;
typedef std::wstringstream TStringStream;
#define to_TString to_wstring
#else
typedef std::string TString;
typedef std::stringstream TStringStream;
#define to_TString to_string
#endif // !_UNICODE

// Duilib
#include "Duilib\header.h"

//rapid json
#include "rapidjson\header.h"

// PUGIXML
#include "pugixml\pugixml.hpp"

// helper
#include "Helper/asyn_task.h"

#include "common.h"

#include "application.h"
#include "main_frame.h"

#define USE(Type) Application::Instance()->Get##Type()
#define DEFINE(Type, name) Type* name = USE(Type);
#define DO(work,...) Application::Instance()->##work(__VA_ARGS__)

#define ASYN(...)   helper_lib::AsyncTask::Post(USE(MainFrame)->GetHWND(), __VA_ARGS__)